@echo off
cls
IF NOT EXIST bin ( mkdir bin )
pushd %CD%
cd bin
set PATH=C:/Qt/Tools/mingw730_64/bin;%PATH%
cmake -DCMAKE_BUILD_TYPE=Debug -DCMAKE_EXPORT_COMPILE_COMMANDS=ON -G "Ninja" ../ || exit /B 1
ninja all || exit /B 1
AOC_2018.exe < ../inputs/day2
popd

