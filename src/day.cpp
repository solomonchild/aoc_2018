#include "day.hpp"

#include <chrono>
#include <fstream>
#include <iostream>

void day::solve() {

  auto fname = "../inputs/day" + std::to_string(static_cast<int>(day_));
  std::ifstream fs(fname);

  std::cout << "Running day " << static_cast<int>(day_) << std::endl;
  using func_type = decltype(&day::part1);

  auto run_part = [this, &fs](int part, func_type&& func) {
    auto get_passed_time = [](auto start) {
      return std::to_string(
                 std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now() - start)
                     .count()) +
             "ms";
    };

    auto start = std::chrono::system_clock::now();

    if (part == 1) prepare(fs);
    auto ans = (this->*func)();
    const auto& expected = (part == 1) ? ans1 : ans2;
    auto equal = expected == ans;
    std::cout << "\t[" << get_passed_time(start) << "]part" << part << ": " << ans;
    if (!expected.empty()) {
      std::cout << ", Expected: " << expected << " " << (equal ? "[PASSED]" : "[FAILED]");
    }
    std::cout << std::endl;
  };
  run_part(1, &day::part1);
  run_part(2, &day::part2);
  std::cout << "----------------" << std::endl;
}
