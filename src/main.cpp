#include "day.hpp"

int main() {
    //solve<aoc_day::day1>("425", "57538");
    //solve<aoc_day::day2>("7688", "lsrivmotzbdxpkxnaqmuwcchj");
    //solve<aoc_day::day3>("111935", "650");
    //solve<aoc_day::day4>("14346", "5705");
    //solve<aoc_day::day5>("10638", "4944");
    //solve<aoc_day::day6>("3251", "47841");
    //solve<aoc_day::day7>("MNOUBYITKXZFHQRJDASGCPEVWL", "893");
    //solve<aoc_day::day8>("41849", "32487");
    //solve<aoc_day::day9>("390592", "3277920293");
    //solve<aoc_day::day10>("", "10521");
    //solve<aoc_day::day11>("235,87,3","234,272,18");
    //solve<aoc_day::day12>("3059", "3650000001776");
    //solve<aoc_day::day13>("118,66", "70,129");
    //solve<aoc_day::day14>("8610321414", "20258123");
    //solve<aoc_day::day15>("257954", "51041");
    //solve<aoc_day::day16>("509", "496");
    //solve<aoc_day::day17>("31934", "24790");
    //solve<aoc_day::day18>("653184", "169106");
    //solve<aoc_day::day19>("1968","21211200");
    //solve<aoc_day::day20>("4025", "8186");
    //solve<aoc_day::day21>("13970209","6267260");
    //solve<aoc_day::day22>("5400", "1048");
    //solve<aoc_day::day23>("294", "88894457");
    //solve<aoc_day::day24>("9328","2172");
    solve<aoc_day::day25>("399");
}
