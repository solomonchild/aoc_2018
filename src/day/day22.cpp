#include "day.hpp"

#include <cassert>
#include <functional>
#include <map>
#include <queue>
#include <set>

static constexpr int TORCH = 1;
static constexpr int GEAR = 2;
static constexpr int NEITHER = 3;
struct vec2 {
    int x, y;
    int item = -1;
    bool operator<(const vec2& vec) const {
        if (x == vec.x && y == vec.y)
            return item < vec.item;
        else if (x == vec.x)
            return y < vec.y;
        else
            return x < vec.x;
    }
    bool operator==(const vec2& vec) const { return x == vec.x && y == vec.y && item == vec.item; }
};
class day22 : public day {
  public:
    static constexpr int ROCKY = 0;
    static constexpr int WET = 1;
    static constexpr int NARROW = 2;
    day22(const std::string& ans1, const std::string& ans2)
        : day(aoc_day::day22, ans1, ans2) {}

    std::string part1() override {
        for (auto x : range(tx + 1)) {
            for (auto y : range(ty + 1)) {
                hash(x, y);
            }
        }
        long long risk = 0;
        for (auto x : range(tx + 1)) {
            for (auto y : range(ty + 1)) {
                type[y][x] = (erosion_lvl[y][x] % 3);
                risk += type[y][x];
            }
        }
        return std::to_string(risk);
    }

    std::map<vec2, vec2> to;
    std::map<vec2, int> dist{{{0, 0, TORCH}, 0}};
    auto get_type(const auto& vec2) { return type[vec2.y][vec2.x]; }
    int shortest_path(int sx, int sy, int ex, int ey) {
        auto get_neighbours = [this](const vec2& v) {
            std::vector<vec2> res;
            auto valid = [this](int x, int y) { return x >= 0 && x < side && y >= 0 && y < side; };
            if (valid(v.x - 1, v.y)) res.push_back({v.x - 1, v.y});
            if (valid(v.x + 1, v.y)) res.push_back({v.x + 1, v.y});
            if (valid(v.x, v.y - 1)) res.push_back({v.x, v.y - 1});
            if (valid(v.x, v.y + 1)) res.push_back({v.x, v.y + 1});
            return res;
        };
        std::set<vec2> visited;
        std::priority_queue<vec2, std::vector<vec2>, std::function<bool(const vec2&, const vec2&)>> to_visit(
            [&](const vec2& v1, const vec2& v2) { return dist[v1] > dist[v2]; });

        to_visit.push({sx, sy, TORCH});
        while (!to_visit.empty()) {
            auto v = to_visit.top();
            to_visit.pop();

            auto item = v.item;
            if (visited.count(v)) {
                continue;
            }

            visited.insert(v);
            v.item = item;
            auto put_v = [&](int it, vec2 n) mutable {
                if ((get_type(v) == WET && it == TORCH) || (get_type(v) == NARROW && it == GEAR) ||
                    (get_type(v) == ROCKY && it == NEITHER))
                    return;

                auto time = 1;
                if (v.item != it) {
                    time += 7;
                }
                auto d = dist[v] + time;
                n.item = it;
                if (dist.find(n) == dist.end() || dist[n] > d) {
                    to[n] = v;
                    dist[n] = d;
                }
                to_visit.push(n);
            };
            for (auto n : get_neighbours(v)) {
                if (visited.count(n)) continue;

                if (n.x == ex && n.y == ey) {
                    if (v.item != TORCH) {
                        put_v(TORCH, n);
                    } else {
                        put_v(v.item, n);
                    }
                } else if (get_type(n) == ROCKY) {
                    if (v.item == NEITHER) {
                        put_v(TORCH, n);
                        put_v(GEAR, n);
                    } else {
                        if (v.item == TORCH) put_v(GEAR, n);
                        else if (v.item == GEAR) put_v(TORCH, n);
                        put_v(v.item, n);
                    }
                } else if (get_type(n) == WET) {
                    if (v.item == TORCH) {
                        put_v(GEAR, n);
                        put_v(NEITHER, n);
                    } else {
                        if (v.item == GEAR) put_v(NEITHER, n);
                        else if (v.item == NEITHER) put_v(GEAR, n);
                        put_v(v.item, n);
                    }
                } else if (get_type(n) == NARROW) {
                    if (v.item == GEAR) {
                        put_v(TORCH, n);
                        put_v(NEITHER, n);
                    } else {
                        if (v.item == TORCH) put_v(NEITHER, n);
                        else if (v.item == NEITHER) put_v(TORCH, n);
                        put_v(v.item, n);
                    }
                }
            }
            if (v.x == ex && v.y == ey) {
                return dist[{ex, ey, TORCH}];
            }
        }
        return dist[{ex, ey, TORCH}];
    }

    std::string part2() override {
        for (auto x : range(side)) {
            for (auto y : range(side)) {
                hash(x, y);
                type[y][x] = (erosion_lvl[y][x] % 3);
            }
        }

        auto res = std::to_string(shortest_path(0, 0, tx, ty));
        vec2 cur{tx, ty, TORCH};
        std::deque<vec2> path{cur};
        while (to.find(cur) != to.end()) {
            path.push_front(to[cur]);
            cur = to[{cur.x, cur.y}];
        }
        for (auto c : path) {
            log("{}: {}", c, dist[c]);
        }
        return res;
    }

    void hash(long x, long y) {
        uint64_t geo_idx = 0;
        if (x == 0 && y == 0)
            geo_idx = 0;
        else if (x == tx && y == ty)
            geo_idx = 0;
        else if (y == 0)
            geo_idx = x * 16807;
        else if (x == 0)
            geo_idx = y * 48271;
        else
            geo_idx = erosion_lvl[y][x - 1] * erosion_lvl[y - 1][x];
        geo_grid[y][x] = geo_idx;
        erosion_lvl[y][x] = (geo_grid[y][x] + depth) % 20183;
    }

    void prepare(std::istream& i) override {
        std::string t1;
        char t2;
        i >> t1 >> depth;
        i >> t1 >> tx >> t2 >> ty;
        side = std::max(tx, ty) + 300;
        geo_grid.resize(side, std::vector<long>(side, 0));
        erosion_lvl.resize(side, std::vector<long>(side, 0));
        type.resize(side, std::vector<long>(side, 0));
    }
    long long depth;
    int tx, ty;
    int side;
    std::vector<std::vector<long>> geo_grid;
    std::vector<std::vector<long>> erosion_lvl;
    std::vector<std::vector<long>> type;
};
std::ostream& operator<<(std::ostream& os, const vec2& v) {
    std::string str;
    if (v.item == TORCH) str = "torch";
    if (v.item == GEAR) str = "gear";
    if (v.item == NEITHER) str = "neither";
    os << v.x << "," << v.y << " it: " << str;
    return os;
}

template <>
void solve<aoc_day::day22>(const std::string& ans1, const std::string& ans2) {
    day22 d(ans1, ans2);
    d.solve();
}
