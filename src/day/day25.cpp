#include "day.hpp"

#include <map>
#include <set>

struct vec4 {
    int a, b, c, d;
    bool operator<(const vec4& v) const {
        if (a != v.a) return a < v.a;
        if (b != v.b) return b < v.b;
        if (c != v.c) return c < v.c;
        if (d != v.d) return d < v.d;
    }
};
using points_t = std::vector<vec4>;
struct UF {
    int N = 10;
    std::vector<int> ids;
    UF(int size) { 
        N = size;
        ids.resize(N, 0); 
        std::iota(ids.begin(), ids.end(), 0);
        unique();
    }

    bool connected(int a, int b) { return find(a) == find(b); }

    std::set<int> un;
    void unite(int a, int b) {
        auto aid = find(a);
        auto bid = find(b);
        if(aid == bid) return;

        log("\tRemoving {}, inserting {}", aid, bid);
        un.erase(aid);
        un.insert(bid);
        for(auto i : range(ids.size())) {
            if(ids[i] == aid) {
                ids[i] = bid;
            }
        }
        N--;
    }

    int count() { return N; }

    int unique() { 
        auto temp = ids;
        for(auto i : temp) {
            std::cout << i << ", ";
        }
        log("");
        std::sort(temp.begin(), temp.end());
        return std::unique(temp.begin(), temp.end()) - temp.begin();
    }

    int find(int a) {
        return ids[a];
    }
};

class day25 : public day {
  public:
    points_t points;
    day25(const std::string& ans1, const std::string& ans2)
        : day(aoc_day::day25, ans1, ans2) {}

    std::string part1() override {
        bool set;
        int count = points.size();
        UF uf(count);
        for (auto i : range(count)) {
            const auto& p1 = points[i];
            for (auto j : range(i + 1, count)) {
                const auto& p2 = points[j];
                auto da = std::abs(p1.a - p2.a);
                auto db = std::abs(p1.b - p2.b);
                auto dc = std::abs(p1.c - p2.c);
                auto dd = std::abs(p1.d - p2.d);
                log("{}-{} = {}", i, j, da + db + dc + dd);
                if (da + db + dc + dd <= 3) {
                    log("Will unite {} with {}", i, j);
                    uf.unite(i, j);
                }
            }
        }
        log("Count {}, size {}", count, uf.un.size());
        return std::to_string(uf.count());
    }

    std::string part2() override { return ""; }

    void prepare(std::istream& i) override {
        char temp;
        int a, b, c, d;
        while (i >> a >> temp >> b >> temp >> c >> temp >> d) {
            points.push_back({a, b, c, d});
        }
    }
};

template <>
void solve<aoc_day::day25>(const std::string& ans1, const std::string& ans2) {
    day25 d(ans1, ans2);
    d.solve();
}
