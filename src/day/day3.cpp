#include "day.hpp"

#include "iteration.hpp"
#include <map>

class day3 : public day {
public:
  day3(const std::string& ans1, const std::string& ans2)
      : day(aoc_day::day3, ans1, ans2) {}

  int claimed = 0;
  std::string part1() override {
    for (auto& [id, c] : cuts) {
      for (auto i : range(c.top, c.top + c.height)) {
        for (auto j : range(c.left, c.left + c.width)) {
          fabric[i][j]++;
          if (fabric[i][j] > 1) {
            c.overlap = true;
            cuts[ids[i][j]].overlap = true;
          }
          ids[i][j] = c.id;
        }
      }
    }
    for (const auto& v : fabric) {
      claimed += std::accumulate(v.begin(), v.end(), 0, [](auto res, auto st) { return res + (st > 1); });
    }
    // 111935
    return std::to_string(claimed);
  }

  std::string part2() override {
    for (auto i : range(fabric.size())) {
      for (auto j : range(fabric[0].size())) {
        if (fabric[i][j] == 1 && !cuts[ids[i][j]].overlap) {
          // 650
          return std::to_string(ids[i][j]);
        }
      }
    }
  }

  void prepare(std::istream& i) override {
    int max_w = 0;
    int max_h = 0;

    for (;;) {
      char _;
      cut c;
      i >> _;
      if (!(i >> c.id)) break;
      i >> _ >> c.left >> _ >> c.top >> _ >> c.width >> _ >> c.height;
      max_h = std::max(max_h, c.top + c.height);
      max_w = std::max(max_h, c.left + c.width);
      cuts[c.id] = c;
    }
    for (auto i = 0; i < max_h; i++) {
      fabric.emplace_back(max_w, 0);
      ids.emplace_back(max_w, 0);
    }
  }

  struct cut {
    int id;
    int left;
    int top;
    int width;
    int height;
    bool overlap = false;
  };
  std::map<int, cut> cuts;
  std::vector<std::vector<int>> fabric;
  std::vector<std::vector<int>> ids;
};

template <>
void solve<aoc_day::day3>(const std::string& ans1, const std::string& ans2) {
  day3 d(ans1, ans2);
  d.solve();
}
