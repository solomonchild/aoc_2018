#include "day.hpp"

#include "iteration.hpp"

class day17 : public day {
public:
  day17(const std::string& ans1, const std::string& ans2)
      : day(aoc_day::day17, ans1, ans2) {}

  const int water_x = 500;
  const int water_y = 0;
  const int padding = 1;
  auto can_stand (int x, int y) {
      return grid[y+1][x] == '~' || grid[y+1][x] == '#';
  }
  auto raining (int x, int y) {
      return grid[y-1][x] == '|';
  }
  int find_left_wall(int x, int y) {
      while((grid[y][x] != '#') && x >= x_to_screen(min_x)) x--;
      if(x >= x_to_screen(min_x)) return x;
      else return -1;
  }
  int find_right_wall(int x, int y) {
      while((grid[y][x] != '#') && x <= x_to_screen(max_x)) x++;
      if(x <= x_to_screen(max_x)) return x;
      else return -1;
  }
  std::pair<int, int> get_ground(int x, int y) {
      const auto orig_x = x;
      if(grid[y][x] == '#') return {x,x};
      
      while((grid[y][x+1] != '#' && can_stand(x, y)) && x <= x_to_screen(max_x)) x++;
      auto to = x;
      x = orig_x;
      while((grid[y][x-1] != '#' && can_stand(x, y)) && x >= x_to_screen(min_x)) x--;
      return {x, to};
  }

  void fill(int from, int to, int y, char f) {
      for(auto i : range(from, to+1)) {
          grid[y][i] = f;
      }
  }
  std::string part1() override {
    const int x = x_to_screen(water_x);
    int y = water_y + 1; 
    grid[y++][x]='|';
    bool down = true;
    for(;;) {
        bool was_up_ordered = false;
        for(auto x :range(0, x_to_screen(max_x))) {
            if(grid[y][x] == '#') continue;
            if(raining(x,y)) {
                if(!can_stand(x,y)) {
                    grid[y][x] = '|';
                }
                else if(grid[y][x] != '~' && grid[y][x] != '#'){
                    auto ft = get_ground(x,y);
                    char f = '~';
                    if(grid[y][ft.first-1] != '#' || grid[y][ft.second+1] != '#') f = '|';
                    fill(ft.first, ft.second, y, f);
                    if(grid[y][ft.first-1] != '#' || grid[y][ft.second+1] != '#') {
                        if(!was_up_ordered)
                            down = true; 
                    }
                    else {
                        
                        down = false;
                        was_up_ordered = true;
                    }
                }
            }
        }
        if(down) y++;
        else y--;
        if(y > y_to_screen(max_y)) break;
    }
    log(grid);
    int res = 0;
    for(const auto& row : grid) {
        for(auto c : row) {
            if(c == '|' || c == '~') res++;
        }
    }
    return std::to_string(res);
  }

  std::string part2() override {
    int res = 0;
    for(const auto& row : grid) {
        for(auto c : row) {
            if(c == '~') res++;
        }
    }
    return std::to_string(res);
  }

  inline int x_to_screen(int x) {
      return x - min_x + 1;
  }
  inline int y_to_screen(int y) {
      return y - min_y + 1;
  }

  void prepare(std::istream& i) override {
      std::string line;
      struct raw{
          char x1;
          int c1;
          char x2;
          int c2, c3;
      };
      std::vector<raw> raw_coords;
      while(std::getline(i, line)) {
          char left, right;
          int l_int, r_int1, r_int2;
          sscanf(line.c_str(), "%c=%d, %c=%d..%d", &left, &l_int, &right, &r_int1, &r_int2);
          raw_coords.push_back({left, l_int, right, r_int1, r_int2});
          if(left == 'x') {
              max_x = std::max(l_int, max_x);
              min_x = std::min(l_int, min_x);
              max_y = std::max(max_y, std::max(r_int1, r_int2));
              min_y = std::min(min_y, std::min(r_int1, r_int2));
          }
          else if(left == 'y') {
              max_y = std::max(l_int, max_y);
              min_y = std::min(l_int, min_y);
              max_x = std::max(max_x, std::max(r_int1, r_int2));
              min_x = std::min(min_x, std::min(r_int1, r_int2));
          }
      }

      grid.resize(y_to_screen(max_y) + padding+1);
      for(auto& r : grid) {
          r = row_t(x_to_screen(max_x)+padding, '.');
      }

      for(auto& c : raw_coords) {
          if(c.x1 == 'x') {
              for(auto i : range(c.c2, c.c3+1)) {
                  grid[y_to_screen(i)][x_to_screen(c.c1)] = '#';
              }
          }
          else {
              for(auto i : range(c.c2, c.c3+1)) {
                  grid[y_to_screen(c.c1)][x_to_screen(i)] = '#';
              }
          }
      }
  }
  int min_x = std::numeric_limits<int>::max();
  int min_y = min_x;
  int max_x = std::numeric_limits<int>::min();
  int max_y = max_x;
  using row_t = std::vector<char>;
  using grid_t = std::vector<row_t>;
  grid_t grid;
};

static std::ostream& operator<<(std::ostream& os, const day17::grid_t& grid) {
    for(const auto& r : grid) {
        for(const auto [i,c] : enumerate(r)) {
            os << c;
        }
        log("");
    }
    return os;
}

template <>
void solve<aoc_day::day17>(const std::string& ans1, const std::string& ans2) {
  day17 d(ans1, ans2);
  d.solve();
}
