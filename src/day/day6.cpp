#include "day.hpp"

#include <cassert>
#include <map>
#include <set>
#include <tuple>

class day6 : public day {
public:
  day6(const std::string& ans1, const std::string& ans2)
      : day(aoc_day::day6, ans1, ans2) {}

  using Tuple = std::tuple<int, int>;
  inline int manhattan(const Tuple& left, const Tuple right) {
    return std::abs(std::get<0>(left) - std::get<0>(right)) + std::abs(std::get<1>(left) - std::get<1>(right));
  }
  std::string part1() override {
    auto solve = [this](int x0, int x1, int y0, int y1) {
      safe = 0;
      std::map<int, int> score;
      for (auto i : range(y0, y1)) {
        for (auto j : range(x0, x1)) {
          int min = std::numeric_limits<int>::max();
          int min_key = -1;
          long long field = 0;
          bool two = false;
          for (auto [k, v] : map) {
            auto curr = manhattan(v, {j, i});
            field+=curr;
            if (curr < min) {
              min = curr;
              min_key = k;
              two = false;
            } else if (curr == min) {
              two = true;
            }
          }
          if(field < 10000) safe++;
          if(!two) score[min_key]++;
        }
      }
      return score;
    };

    auto score1 = solve(-500, 500, -500, 500);
    auto score2 = solve(-1100, 1100, -1100, 1100);
    for(auto[k,v]:score1) {
      if(score2[k] != v) {
        score1[k]=0;
      }
    }
    auto it = std::max_element(score1.begin(), score1.end(), [&score1, &score2](const auto& p1, const auto& p2){
        return p1.second < p2.second;
    });

    return std::to_string(it->second);//3251
  }

  std::string part2() override { 
    return std::to_string(safe);  //47841
  }

  void prepare(std::istream& i) override {
    int x, y;
    char skip;
    int key = 0;
    while (i.good()) {
      if (!(i >> x)) break;
      i >> skip >> y;
      map[key++] = {x, y};
    }
  }
  std::map<int, Tuple> map;
  unsigned safe;
};

template <>
void solve<aoc_day::day6>(const std::string& ans1, const std::string& ans2) {
  day6 d(ans1, ans2);
  d.solve();
}
