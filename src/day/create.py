

for i in range(5, 25):
    with open(f"day{i}.cpp", "wb") as f:
        s = fR"""#include "day.hpp"

#include "iteration.hpp"

class day{i} : public day {{
public:
  day{i}()
      : day(aoc_day::day{i}) {{}}

  std::string part1() override {{
    return "";
  }}

  std::string part2() override {{
    return "";
  }}

  void prepare(std::istream& i) override {{
  }}
}};

template <>
void solve<aoc_day::day{i}>() {{
  day{i} d;
  d.solve();
}}"""
        f.write(s.encode())
