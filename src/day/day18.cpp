#include "day.hpp"

#include "iteration.hpp"
#include <array>
#include <set>

class day18 : public day {
public:
  static constexpr int SIDE = 50;
  using row_t = std::array<char, SIDE>;
  using grid_t = std::array<row_t, SIDE>;

  day18(const std::string& ans1, const std::string& ans2)
      : day(aoc_day::day18, ans1, ans2) {}

  void process (int step, grid_t& grid) {
      auto get_neighbours = [&](int x, int y){
          std::string res;
          auto valid = [&](int x, int y) {
              return x >= 0 && x < SIDE && y >= 0 && y < SIDE;
          };
          if(valid(x-1,y)) res += grid[y][x-1];
          if(valid(x+1,y)) res += grid[y][x+1];
          if(valid(x,y-1)) res += grid[y-1][x];
          if(valid(x,y+1)) res += grid[y+1][x];

          if(valid(x-1,y-1)) res += grid[y-1][x-1];
          if(valid(x-1,y+1)) res += grid[y+1][x-1];
          if(valid(x+1,y-1)) res += grid[y-1][x+1];
          if(valid(x+1,y+1)) res += grid[y+1][x+1];
          return res;
      };
      auto adjacent_trees = [&](int x, int y) {
          auto ns = get_neighbours(x,y);
          return std::count(ns.begin(), ns.end(), TREE);
      };
      auto adjacent_lumber = [&](int x, int y) {
          auto ns = get_neighbours(x,y);
          return std::count(ns.begin(), ns.end(), LUMBER);
      };
      auto adjacent_open = [&](int x, int y) {
          auto ns = get_neighbours(x,y);
          return std::count(ns.begin(), ns.end(), OPEN);
      };
      int cycle = 0;
      for(auto i : range(step)) {
          auto next_grid = grid;
          for(auto y : range(SIDE)) {
              for(auto x : range(SIDE)) {
                  auto tile = grid[y][x];
                  auto tree_num = adjacent_trees(x,y);
                  auto open_num = adjacent_open(x,y);
                  auto lumber_num = adjacent_lumber(x,y);
                  if(tile == OPEN &&  tree_num >= 3) next_grid[y][x] = TREE;
                  else if(tile == TREE && lumber_num >= 3) next_grid[y][x] = LUMBER;
                  else if(tile == LUMBER) {
                      if(lumber_num >= 1 && tree_num >= 1)
                          next_grid[y][x] = LUMBER;
                      else next_grid[y][x] = OPEN;
                  } 
              }
          }
          grid=next_grid;
      }
  }
  std::string part1() override {
      std::set<grid_t> results;
      int cycle = 0;
      grid_t local_grid = grid;
      process(10, local_grid);
      return std::to_string(get_sum(local_grid));
  }

  auto get_sum(const grid_t& local_grid) -> long long {
    long long lumbers = 0;
    long long wooden = 0;
    for(const auto& row : local_grid) {
        for(auto c : row) {
            if(c == TREE) wooden++;
            else if(c == LUMBER) lumbers++;
        }
    }
    return lumbers*wooden;

  }
  std::string part2() override { 

      std::set<grid_t> results;
      int cycle = 0;
      int cycle_start = 0;
      grid_t local_grid = grid;
      for(auto i : range(1000000000))
      {
          process(1, local_grid);
          if(auto [it, r] = results.insert(local_grid); !r) {
              log("Repetition on iteration {}", i);
              results.clear();
              if(cycle_start == 0) {
                  cycle_start = i;
              }
              else { cycle = i - cycle_start; break; }
          }
      }
      log("Cycle {}", cycle);
      local_grid = grid;
      process(cycle_start + (1000000000-cycle_start)%cycle, local_grid);
      return std::to_string(get_sum(local_grid)); 
  }

  void prepare(std::istream& i) override {
    std::string line;
    int row = 0;
    while (std::getline(i, line)) {
      std::copy(line.begin(), line.end(), grid[row].begin());
      row++;
    }
  }
  static const char TREE = '|';
  static const char LUMBER = '#';
  static const char OPEN = '.';
  grid_t grid = {{}};
};

std::ostream& operator<<(std::ostream& os, const day18::grid_t& grid) {
  int i = 0;
  for (auto row : grid) {
    for (auto c : row) {
      os << c;
    }
    i++;
    if (i != grid.size()) log("");
  }
  return os;
}

template <>
void solve<aoc_day::day18>(const std::string& ans1, const std::string& ans2) {
  day18 d(ans1, ans2);
  d.solve();
}
