#include "day.hpp"

#include <cctype>
#include <cmath>

class day5 : public day {
public:
  day5(const std::string& ans1, const std::string& ans2)
      : day(aoc_day::day5, ans1, ans2) {}

  auto react(const std::string& str) {
    std::string s;
    const auto diff = std::abs('A'-'a');
    for(auto c : str) {
      if(!s.empty() && std::abs(s.back() - c) == diff) {
        s.pop_back();
      }
      else s += c; 
    }
    return s;
  }

  std::string part1() override {
    size_t i = 0;
    return std::to_string(react(line).size());
  }

  std::string remove_all_case(const std::string& s, char c) {
    char c1 = std::tolower(c);
    char c2 = std::toupper(c);
    std::string res;
    for (auto i : range(s.size())) {
      if (s[i] != c1 && s[i] != c2) {
        res += s[i];
      }
    } 
    return res; //10638
  }

  std::string part2() override {
    int min_len = line.size();

    auto line_orig = line;

    for (auto c : range('A', 'Z')) {
      line = remove_all_case(line_orig, c);
      auto cur = std::stol(part1());
      min_len = std::min<size_t>(cur, min_len);
    }
    return std::to_string(min_len); //4944
  }

  void prepare(std::istream& i) override { std::getline(i, line); }

private:
  std::string line;
};

template <>
void solve<aoc_day::day5>(const std::string& ans1, const std::string& ans2) {
  day5 d(ans1, ans2);
  d.solve();
}
