#include "day.hpp"

#include "iteration.hpp"
#include <cassert>
#include <unordered_set>

const static std::unordered_set<char> cart_symbols = {'^', '<', '>', 'v'};
class day13 : public day {
public:
  day13(const std::string& ans1, const std::string& ans2)
      : day(aoc_day::day13, ans1, ans2) {}

  void solve_both() {
    auto active = carts.size();
    const auto total_carts = active;
    while (active > 1) {
        std::sort(carts.begin(), carts.end(), [](const auto& c1, const auto& c2){ 
                if(c1.y == c2.y) return c1.x < c2.x;
                else return c1.y < c2.y;
        });
        for(auto& crt1 : carts) {
            if(!crt1.active) continue;
            crt1.move();
            for(auto& crt2 : carts) {
                if(!crt2.active || crt1 == crt2) continue;
                if(crt1.x == crt2.x && crt1.y == crt2.y){
                    crt1.active = false;
                    crt2.active = false;
                    if(active == total_carts) crash = std::pair(crt1.x, crt2.y);
                    active-=2;
                    break;
                }
            }
        }
      }
    auto crt = *std::find_if(carts.begin(), carts.end(), [](const auto& crt){ return crt.active; });
    last_cart = std::pair(crt.x, crt.y);
  }
  std::string part1() override {
    solve_both();
    return std::to_string(crash.first)+","+std::to_string(crash.second);; 
  }

  std::string part2() override { 
    return std::to_string(last_cart.first)+","+std::to_string(last_cart.second);; 
  }

  void print_grid() {
    for (const auto& [y, row] : enumerate(m)) {
      for (auto [x, c] : enumerate(row)) {
        bool cart_printed = false;
        for (const auto& crt : carts) {
          if (crt.x == x && crt.y == y && crt.active) {
            std::cout << cart::dir_to_str(crt.d);
            cart_printed = true;
          }
        }
        if (!cart_printed) std::cout << c;
      }
      log("");
    }
  }

  void prepare(std::istream& i) override {
    std::string line;
    while (std::getline(i, line)) {
      m.emplace_back(line.begin(), line.end());
      for (auto [i, c] : enumerate(*m.rbegin())) {
        if (cart_symbols.count(c) != 0) {
          int y = m.size() - 1;
          auto& ch = (*m.rbegin())[i];
          carts.emplace_back(i, y, c, m, carts);
          if (ch == 'v' || ch == '^') ch = '|';
          if (ch == '>' || ch == '<') ch = '-';
        }
      }
    }
  }
  using row = std::vector<char>;
  using map = std::vector<row>;
  struct cart {
    enum dir { up, right, down, left, END };
    static dir char_to_dir(char chr) {
      if (chr == '^') return dir::up;
      else if (chr == '>') return dir::right;
      else if (chr == '<') return dir::left;
      else return dir::down;
    }

    static std::string dir_to_str(dir d) {
      if (d == dir::up) return "^";
      else if (d == dir::down) return "v";
      else if (d == dir::right) return ">";
      else return "<";
    }

    bool move() {
      auto chr = grid[y][x];
      if (chr == '/') {
        if (d == dir::up) d = dir::right;
        else if (d == dir::down) d = dir::left;
        else if (d == dir::right) d = dir::up;
        else if (d == dir::left) d = dir::down;
      } else if (chr == '\\') {
        if (d == dir::up) d = dir::left;
        else if (d == dir::down) d = dir::right;
        else if (d == dir::right) d = dir::down;
        else if (d == dir::left) d = dir::up;
      } else if (chr == '+') {
        if (moves[m] == dir::right) turn_right();
        else if (moves[m] == dir::left) turn_left();
        next_move();
      }
      if (d == dir::right) x++;
      else if (d == dir::left) x--;
      else if (d == dir::up) y--;
      else if (d == dir::down) y++;
      return true;
    }

    void turn_right() {
          d = dir((d + 1) % dir::END);
    }
    void turn_left() {
          d = dir(((d - 1) + dir::END) % dir::END);
    }
    std::array<dir, 3> moves = {dir::left, dir::END, dir::right};
    void next_move() { m = (m + 1) % moves.size(); }


    friend std::ostream& operator<<(std::ostream& os, cart c) {
      os << c.x << ", " << c.y << dir_to_str(c.d);
      return os;
    }

    cart(int x, int y, char c, map& m, std::vector<cart>& carts)
        : x(x)
        , y(y)
        , d(char_to_dir(c))
        , m(0)
        , grid(m)
        , carts(carts)
        ,id(idx++) {}

    cart(const cart& c) = default;
    cart& operator=(const cart& c) 
    {
        x=c.x;
        y=c.y;
        d=c.d;
        m=c.m;
        grid=c.grid;
        carts=c.carts;
        id=c.id;
        active=c.active;
        return *this;
    }
    int x, y, id;
    dir d;
    int m;
    map& grid;
    std::vector<cart>& carts;
    bool active = true;
    bool operator==(const cart& other) const {return id == other.id;}
    bool operator!=(const cart& other) const {return !(*this==other);}

    static int idx;
  };
  std::pair<int, int> crash;
  std::pair<int, int> last_cart;
  std::vector<cart> carts;
  map m;
};
int day13::cart::idx = 0;

template <>
void solve<aoc_day::day13>(const std::string& ans1, const std::string& ans2) {
  day13 d(ans1, ans2);
  d.solve();
}
