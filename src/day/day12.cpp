#include "day.hpp"
#include <unordered_map>

class day12 : public day {
public:
  day12(const std::string& ans1, const std::string& ans2)
      : day(aoc_day::day12, ans1, ans2) {}

  auto calc_sum(const std::string& line) {
    int idx = left;
    int total = 0;
    for (auto c : line) {
      if (c == '#') {
        total += idx;
      }
      idx++;
    }
    return total;
  }

  std::string part1() override {
    auto line = starting_line;
    auto next_line = line;
    auto prev_sum = 0;
    for (auto i : range(20)) {
      for (auto i : range(line.size())) {
        if (line.find('#') <= 2) {
          line = std::string(5, '.') + line;
          left -= 5;
        }
        if (auto idx = line.rfind('#'); (idx >= line.size() - 3) && idx != std::string::npos) {
          line = line + std::string(6, '.');
        }
        next_line = line;

        auto left = line.substr(i, 5);
        next_line.replace(i, 5, ".....");
        if (auto it = rules.find(left); it != rules.end()) {
          next_line.replace(i + 2, 1, "#");
        }
      }
      for (const auto& p : rules) {
        int idx = 0;
        while (idx = line.find(p.first, idx), idx != std::string::npos) {
          next_line.replace(idx + 2, 1, p.second);
          idx++;
        }
      }
      line = next_line;
    }
    return std::to_string(calc_sum(next_line));
  }

  std::string part2() override { return std::to_string(10317 + (50000000000 - 116 - 1) * 73); }

  void prepare(std::istream& i) override {
    i >> starting_line >> starting_line >> starting_line;
    starting_line = starting_line;
    std::string left;
    while (i >> left) {
      std::string right;
      std::string c;
      i >> c >> right;
      right = right;
      rules[left] = right;
    }
  }
  std::unordered_map<std::string, std::string> rules;
  std::string starting_line;
  int left = 0;
};

template <>
void solve<aoc_day::day12>(const std::string& ans1, const std::string& ans2) {
  day12 d(ans1, ans2);
  d.solve();
}
