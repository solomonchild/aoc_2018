#include "day.hpp"
#include <unordered_set>

class day1 : public day {
public:
  day1(const std::string& ans1, const std::string& ans2) : day(aoc_day::day1, ans1, ans2) {}

  std::string part1() override {
    auto ans = std::accumulate(vec.begin(), vec.end(), 0);
    return std::to_string(ans);
  }

  std::string part2() override {
    std::unordered_set<int> set;
    auto current = 0;
    auto current_idx = 0u;
    for (;;) {
      current += vec[(current_idx++) % vec.size()];
      auto [_, was_inserted] = set.insert(current);
      if (!was_inserted) {
        break;
      }
    }
    return std::to_string(current);
    // assert(current == 57538);
  }
  void prepare(std::istream& i) override {
    std::istream_iterator<int> ii(i);
    std::copy(ii, {}, std::back_inserter(vec));
  }
  std::vector<int> vec;
};

template <>
void solve<aoc_day::day1>(const std::string& ans1, const std::string& ans2) {
  day1 d(ans1, ans2);
  d.solve();
}
