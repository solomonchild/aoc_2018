#include "day.hpp"

#include <cassert>
#include <cstdio>
#include <queue>
#include <unordered_map>
#include <unordered_set>

class day7 : public day {
public:
  day7(const std::string& ans1, const std::string& ans2)
      : day(aoc_day::day7, ans1, ans2) {}

  std::string part2() override {

    auto no_incoming = no_incoming_;
    auto incoming = incoming_;

    auto any_idle = [this]() {return std::any_of(time_workers.begin(), time_workers.end(), [](int i ){return i == 0;});};
    auto any_active = [this]() {return std::any_of(time_workers.begin(), time_workers.end(), [](int i ){return i != 0;});};

    auto get_idx_of_idle=[this]() -> int{
      auto it = std::find(time_workers.begin(), time_workers.end(), 0);
      if(it == time_workers.end()) return - 1;
      auto idx = std::distance(time_workers.begin(), it);
      return idx;
    };
    std::string res;
    auto get_chars = [&,this](){
      for(auto idx: range(workers)) {
        if(time_workers[idx] || char_workers[idx] == 0) continue;
        if( char_workers[idx]!=0 ) { 
          auto c = char_workers[idx];
          res += c;
          char_workers[idx]=0;
          for(char n : before_after[c]) {
              assert(incoming[n].erase(c) != 0);
              if(incoming[n].empty()) no_incoming.push(n);
          }
        }
      }
    };
    while(any_active() || !no_incoming.empty()) {
      while(any_idle() && !no_incoming.empty()) {
        auto c = no_incoming.top(); no_incoming.pop();
        auto idx = get_idx_of_idle();
        char_workers[idx] = c;
        time_workers[idx] = t + 1 + c-'A';
      }
      total_time++;
      std::for_each(time_workers.begin(), time_workers.end(), [](int& i){ if(i) i--; });
      get_chars();
    }
    return std::to_string(total_time);
  }

  std::string part1() override { 
    
    auto no_incoming = no_incoming_;
    auto incoming = incoming_;
    std::string res;

    while(!no_incoming.empty()) {
      auto c = no_incoming.top();
      no_incoming.pop();
      assert(incoming[c].empty());
      res += c;

      for(char n : before_after[c]) {
          assert(incoming[n].erase(c) != 0);
          if(incoming[n].empty()) no_incoming.push(n);
      }
    }
    return res;
  }

  void prepare(std::istream& i) override {
    std::string line;
    while (std::getline(i, line)) {
      char before, after;
      sscanf(line.c_str(), "Step %c must be finished before step %c can begin.", &before, &after);
      before_after[before].push_back(after);
      incoming_[after].insert(before);
    }
    for(auto[k,v] : before_after) {
      if (incoming_[k].empty()) no_incoming_.push(k);
    }
  }
  std::unordered_map<char, std::vector<char>> before_after;
  std::unordered_map<char, std::unordered_set<char>> incoming_;
  std::priority_queue<char, std::vector<char>, std::greater<char>> no_incoming_;
  unsigned total_time = 0;
  constexpr static int workers = 5;
  constexpr static int t = 60;
  std::array<int, workers> time_workers={{}};
  std::array<char, workers> char_workers={{}};
};

template <>
void solve<aoc_day::day7>(const std::string& ans1, const std::string& ans2) {
  day7 d(ans1, ans2);
  d.solve();
}
