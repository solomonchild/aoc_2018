#include "day.hpp"

#include "iteration.hpp"
#include <array>
#include <thread>

class day10 : public day {
public:
  struct point {
    int pos_x = 0;
    int pos_y = 0;
    int vel_x = 0;
    int vel_y = 0;
  };
  day10(const std::string& ans1, const std::string& ans2)
      : day(aoc_day::day10, ans1, ans2) {}

  std::string part1() override {
    uint64_t area = 0;
    auto prev_area = std::numeric_limits<uint64_t>::max();
    std::vector<point> points_prev;
    int p_min_x, p_min_y, p_max_x, p_max_y;
    for (;;) {
      int min_x = std::numeric_limits<int>::max();
      int max_x = std::numeric_limits<int>::min();
      int min_y = min_x;
      int max_y = max_x;
      for (auto& p : points) {
        p.pos_x = (p.pos_x + p.vel_x);
        p.pos_y = (p.pos_y + p.vel_y);

        min_x = std::min(min_x, p.pos_x);
        max_x = std::max(max_x, p.pos_x);
        min_y = std::min(min_y, p.pos_y);
        max_y = std::max(max_y, p.pos_y);
      }
      std::tie(p_min_x, p_max_x, p_min_y, p_max_y) = std::tie(min_x, max_x, min_y, max_y);
      secs++;
      area = uint64_t(std::abs(max_x - min_x)) * uint64_t(std::abs(max_y - min_y));
      if (area > prev_area) {
        std::vector<std::vector<char>> grid(p_max_y + 1);
        for (auto& r : grid)
          r.resize(p_max_x + 1, ' ');

        for (const auto& p : points_prev) 
          grid[p.pos_y][p.pos_x] = '#';

        std::ofstream of("msg.txt");
        for (const auto& r : grid) {
          for (const auto c : r) {
            of << c << " ";
          }
          of << std::endl;
        }
        return "See msg.txt";
      }
      prev_area = area;
      points_prev = points;
    }
  }

  std::string part2() override { return std::to_string(secs - 1); }

  void prepare(std::istream& i) override {
    std::string line;
    while (std::getline(i, line)) {
      point p;
      std::sscanf(line.c_str(), "position=< %d,  %d> velocity=< %d,  %d>", &p.pos_x, &p.pos_y, &p.vel_x, &p.vel_y);
      points.push_back(p);
    }
  }
  std::vector<point> points;
  int secs = 0;
};

template <>
void solve<aoc_day::day10>(const std::string& ans1, const std::string& ans2) {
  day10 d(ans1, ans2);
  d.solve();
}
