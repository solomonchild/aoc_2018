#include "day.hpp"

#include "iteration.hpp"

class day11 : public day {
public:
  day11(const std::string& ans1, const std::string& ans2)
      : day(aoc_day::day11, ans1, ans2) {}


  int calculate_cell(int y, int x) {
    auto rack_id = x + 10;
    auto power_lvl = (rack_id * (y) + serial_number) * rack_id;
    power_lvl = (power_lvl < 100) ? 0 : std::to_string(power_lvl)[std::to_string(power_lvl).size() - 3] - '0';
    power_lvl -= 5;
    return power_lvl;
  }
  int calculate_grid(int y, int x, int size = 1) {
    auto total = 0;
    for (auto i : range(size)) {
      for (auto j : range(size)) {
        total += grid[i + y][j + x];
      }
    }
    return total;
  }

  template <typename C>
  std::string solve_for(const C& c) {
    int max = std::numeric_limits<int>::min();
    int max_x, max_y, max_size;
    for (auto k : c) {
      for (auto i : range(1, SIDE - k)) {
        for (auto j : range(1, SIDE - k)) {
          auto cur = calculate_grid(i, j, k);
          if (cur > max) std::tie(max, max_x, max_y, max_size) = std::tie(cur, j, i, k);
        }
      }
    }
    return std::to_string(max_x) + "," + std::to_string(max_y) + "," + std::to_string(max_size);
  }
  std::string part1() override { return solve_for(range(3, 4)); }

  std::string part2() override { return solve_for(range(1, SIDE + 1)); }

  void prepare(std::istream& i) override {
    i >> serial_number;
    for (auto i : range(1, SIDE)) {
      for (auto j : range(1, SIDE)) {
        grid[i][j] = calculate_cell(i, j);
      }
    }
  }
  int serial_number = 0;
  constexpr static int SIDE = 300;
  std::array<std::array<int, SIDE+1>, SIDE+1> grid = {{}};
};

template <>
void solve<aoc_day::day11>(const std::string& ans1, const std::string& ans2) {
  day11 d(ans1, ans2);
  d.solve();
}
