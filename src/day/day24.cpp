#include "day.hpp"

#include <cassert>
#include <regex>
#include <set>

class day24 : public day {
  public:
    enum attack_t { radiation = 0x01, fire = 0x02, cold = 0x04, bludge = 0x08, slashing = 0x10 };
    struct team_t {

        team_t(){}
        team_t(const team_t& other){
            name = other.name;
            groupno = other.groupno;
            units = other.units;
            hp = other.hp;
            immune = other.immune;
            weak = other.weak;
            attack = other.attack;
            in = other.in;
            dmg = other.dmg;
        
        
        }
        std::string name;
        int groupno = 0;
        int units = 0;
        int hp = 0;
        int immune = 0;
        int weak = 0;
        int attack = 0;
        int in = 0;
        int dmg = 0;
        int get_power() const { return dmg * units; }
        std::string attack_desc() const { return desc(attack); }
        std::string immune_desc() const { return desc(immune); }
        std::string weak_desc() const { return desc(weak); }
        std::string desc(int a) const {
            std::string res;
            if (a & radiation) res += ", radiation";
            if (a & fire) res += ", fire";
            if (a & cold) res += ", cold";
            if (a & bludge) res += ", bludge";
            if (a & slashing) res += ", slashing";
            return res.empty() ? "" : res.substr(1);
        }
    };
    using map_t = std::map<int, std::shared_ptr<team_t>>;
    using teams_t = std::vector<std::shared_ptr<team_t>>;
    using selection_t = std::map<std::shared_ptr<team_t>, std::shared_ptr<team_t>>;
    map_t orig_immunity;
    map_t orig_infection;
    day24(const std::string& ans1, const std::string& ans2)
        : day(aoc_day::day24, ans1, ans2) {}

    teams_t in_dec_power(map_t& map, map_t& other_map) {
        teams_t res;
        std::transform(map.begin(), map.end(), std::back_inserter(res), [](const auto& p) { return p.second; });
        std::transform(other_map.begin(), other_map.end(), std::back_inserter(res), [](const auto& p) { return p.second; });

        std::sort(res.begin(), res.end(), [](const auto& t1, const auto& t2) {
            if (t1->get_power() == t2->get_power()) {
                return t1->in > t2->in;
            } else
                return t1->get_power() > t2->get_power();
        });
        return res;
    }
    teams_t get_attacking_order(map_t& map1, map_t& map2) {
        teams_t res;
        std::transform(map1.begin(), map1.end(), std::back_inserter(res), [](const auto& p) { return p.second; });
        std::transform(map2.begin(), map2.end(), std::back_inserter(res), [](const auto& p) { return p.second; });
        std::sort(res.begin(), res.end(), [](const auto& t1, const auto& t2) { return t1->in > t2->in; });
        return res;
    }
    int get_attack_power(const team_t& t1, const team_t& t2) {
        int power = t1.get_power();
        if (t1.attack & t2.weak) power *= 2;
        if (t1.attack & t2.immune) power = 0;
        return power;
    }
    teams_t order_targets(const team_t& team, const map_t& map) {
        teams_t res;
        std::transform(map.begin(), map.end(), std::back_inserter(res), [](const auto& p) { return p.second; });
        res.erase(std::remove_if(res.begin(), res.end(), [](const auto& p) { return p->units == 0; }), res.end());
        std::sort(res.begin(), res.end(), [&team, this](const auto& t1, const auto& t2) {
            auto power1 = get_attack_power(team, *t1);
            auto power2 = get_attack_power(team, *t2);
            if (power1 == power2) {
                if (t1->get_power() == t2->get_power()) {
                    return t1->in > t2->in;
                } else
                    return t1->get_power() > t2->get_power();
            } else
                return power1 > power2;
        });
        return res;
    }

    selection_t assign_targets(map_t& immunity, map_t& infection) {
        selection_t targets;
        std::set<std::shared_ptr<team_t>> taken;
        auto attackers = in_dec_power(immunity, infection);
        for (auto attacker : attackers) {
            assert(attacker != nullptr);
            assert(attacker->units);

            teams_t to_target; 
            if(attacker->name=="Immunity") to_target = order_targets(*attacker, infection);
            else to_target = order_targets(*attacker, immunity);
            for (auto target : to_target) {
                assert(target->units);
                if (taken.count(target)) continue;
                if(get_attack_power(*attacker, *target) == 0) continue;

                targets[attacker] = target;
                taken.insert(target);
                break;
            }
        }
        return targets;
    }

    bool all_dead(const map_t& teams) {
        auto res = std::all_of(teams.begin(), teams.end(), [](const auto& p) { return p.second->units == 0; });
        return res;
    }

    int print_groups(map_t& map, std::string what) {
        int res = 0;
        log(what);
        if(map.empty()) log("No groups remain.");
        for (const auto& [n, group] : map) {
            log("{} group {} has {} units", group->name, group->groupno, group->units);
            res += group->units;
        }
        log("");
        return res;
    }
    std::tuple<map_t, map_t, bool> proceed(int boost = 0) {
        map_t immunity;
        for(auto[k,v] : orig_immunity) {
            immunity[k] = std::make_shared<team_t>(*v);
            immunity[k]->dmg += boost;
        }
        map_t infection;
        for(auto[k,v] : orig_infection) {
            infection[k] = std::make_shared<team_t>(*v);
        }
        int total_kill = 0;
        while (!all_dead(immunity) && !all_dead(infection)) {
            auto targets = assign_targets(immunity, infection);
            auto attackers = get_attacking_order(infection, immunity);

            total_kill = 0;
            for (auto attacker : attackers) {
                auto target = targets[attacker];
                if (target == nullptr) continue;
                assert(target->units);
                auto power = get_attack_power(*attacker, *target);
                auto killed = std::min(power / target->hp, target->units);
                //log("power is {}, target's hp is {}", power, target->hp);
                target->units -= killed;
                total_kill += killed;

                //log("{} group {} attacks defending group {}, killing {} units", attacker->name, attacker->groupno,
                    //target->groupno, killed);
                if (!target->units) {
                    if (target->name == "Infection")
                        infection.erase(target->groupno);
                    else
                        immunity.erase(target->groupno);
                }
            }
            if(!total_kill) break;
        }
        log("");
        return {immunity, infection, total_kill != 0};
    }
    std::string part1() override {
        auto tuple = proceed();
        auto res = print_groups(std::get<0>(tuple), "Immune System");
        res = std::max(res, print_groups(std::get<1>(tuple), "Infection"));
        return std::to_string(res);
    }

    std::string part2() override { 
        auto res = 0;
        int lo = 0;
        int hi = 1800;
        int last_non_zero = 0;
        int target = 0;
        bool lo_adjusted = false;
        while(lo<=hi) {
            auto mid = (lo+hi)/2;
            auto tuple = proceed(mid);
            if(std::get<2>(tuple) == false) {
                if(lo_adjusted) lo ++;
                else hi--;
                continue;
            }
            res = print_groups(std::get<0>(tuple), "Immune System");
            if(res > 0) {
                last_non_zero = res;
                hi = (lo + hi)/2 - 1;
                lo_adjusted = false;
            }
            else {
                lo = (lo + hi)/2 + 1;
                lo_adjusted = true;
            }
        }
        if(res != 0)
            return std::to_string(res);
        else 
            return std::to_string(last_non_zero);
    }

    std::vector<std::string> get_matches(const std::string& line, const std::regex& re) {
        std::vector<std::string> res;
        for (auto it = std::sregex_iterator(line.begin(), line.end(), re); it != std::sregex_iterator(); it++) {
            std::smatch match = *it;
            for (auto [i, sit] : enumerate(match)) {
                res.push_back(sit);
            }
        }
        return res;
    }

    int attack_to_int(std::string type) {
        if (type == "radiation") return radiation;
        if (type == "fire") return fire;
        if (type == "cold") return cold;
        if (type == "bludgeoning") return bludge;
        if (type == "slashing") return slashing;
        return 0;
    }

    void prepare(std::istream& i) override {

        std::string line;
        std::regex re(
            "(\\d+) units each with (\\d+) hit points (?:(?:\\(([^;]*)?(?:; )?(.*)\\)) )?with an attack that does (\\d+) "
            "(\\w+) damage at initiative (\\d+)");
        bool do_immunity = false;
        int groupno = 1;
        while (std::getline(i, line)) {
            if (line.find("Immune") != std::string::npos) {
                do_immunity = true;
                groupno = 1;
                continue;
            } else if (line.find("Infection") != std::string::npos) {
                do_immunity = false;
                groupno = 1;
                continue;
            }
            log(line);
            team_t t;
            t.groupno = groupno++;
            auto mtchs = get_matches(line, re);
            if (mtchs.begin() == mtchs.end()) continue;
            for (auto [i, s] : enumerate(mtchs)) {
                if (i == 0) continue;
                if (i == 1) {
                    t.units = std::stol(s);
                } else if (i == 2) {
                    t.hp = std::stol(s);
                } else if (s.find("immune") != std::string::npos) {
                    std::regex re("\\w+");
                    int i = 0;
                    log(s);
                    for (auto m : get_matches(s, re)) {
                        if (i++ >= 2) {
                            t.immune |= attack_to_int(m);
                        }
                    }
                } else if (s.find("weak") != std::string::npos) {
                    std::regex re("\\w+");
                    int i = 0;
                    log(s);
                    for (auto m : get_matches(s, re)) {
                        if (i++ >= 2) {
                            t.weak |= attack_to_int(m);
                        }
                    }
                } else if (i == 5) {
                    t.dmg = std::stol(s);
                } else if (i == 6) {
                    t.attack = attack_to_int(s);
                } else if (i == 7) {
                    t.in = std::stol(s);
                }
            }
            if (do_immunity) {
                t.name = "Immunity";
                log("Pushed {}", t);
                orig_immunity[t.groupno] = std::make_shared<team_t>(t);
            } else {
                t.name = "Infection";
                log("Pushed {}", t);
                orig_infection[t.groupno] = std::make_shared<team_t>(t);
            }
        }
        log("team Imm:\n {}", orig_immunity);
        log("team Inf:\n {}", orig_infection);
    }
};
std::ostream& operator<<(std::ostream& os, const day24::team_t& team) {
    os << team.units << " units, " << team.hp << " HP, " << team.dmg << ", in: " << team.in;
    os << ", dmg: " << team.dmg << " of " << team.attack_desc();
    os << ", immune: " << team.immune_desc() << ", weak: " << team.weak_desc();
    return os;
}
std::ostream& operator<<(std::ostream& os, const day24::map_t& teams) {
    for (auto [k, t] : teams) {
        os << *t << std::endl;
    }
    return os;
}
template <>
void solve<aoc_day::day24>(const std::string& ans1, const std::string& ans2) {
    day24 d(ans1, ans2);
    d.solve();
}
