#include "day.hpp"

#include <queue>
#include <functional>

struct vec3 {
    int x = 0;
    int y = 0;
    int z = 0;
    vec3& operator=(const vec3& other) { x = other.x, y = other.y, z = other.z; }
    vec3 operator-(const vec3& other) const { return {x - other.x, y - other.y, z - other.z}; }
    vec3 operator+(const vec3& other) const { return {x + other.x, y + other.y, z + other.z}; }
    vec3 operator/(int i) const { return {x/i, y/i, z/i}; }
    vec3 operator*(vec3 v) const { return {x*v.x, y*v.y, z*v.z}; }
    void operator/=(int i) { *this = operator/(i); }
    int abs() const { return std::abs(x) + std::abs(y) + std::abs(z); }
};

class day23 : public day {
  public:
    struct pod {
        vec3 pos;
        int r;
    };
    day23(const std::string& ans1, const std::string& ans2)
        : day(aoc_day::day23, ans1, ans2) {}

    auto mdist(const pod& p1, const pod& p2) { return (p1.pos - p2.pos).abs(); }
    auto mdist(vec3 v, const pod& p2) { return (v - p2.pos).abs(); }
    auto mdist(vec3 v1, vec3 v2) { return (v1 - v2).abs(); }
    std::string part1() override {
        largest_pod =
            *std::max_element(pods.begin(), pods.end(), [](const auto& p1, const auto& p2) { return p1.r < p2.r; });
        int res = 0;
        for (const auto& p : pods) {
            if (mdist(p, largest_pod) <= largest_pod.r) res++;
        }

        return std::to_string(res);
    }

    struct box_t {
        vec3 lo;
        vec3 hi;
    };
    using boxes_t = std::vector<box_t>;
    boxes_t subdivide(box_t box) {
        boxes_t boxes;
        auto dist = (box.hi - box.lo).abs()/2;
        vec3 octants[] = {
            {0,0,1},
            {0,1,0},
            {1,0,0},
            {0,1,1},
            {1,1,0},
            {1,0,1},
            {1,1,1},
        };
        for(auto o : octants) {
            auto i = o*vec3{dist,dist,dist};
            boxes.push_back(box_t{box.lo+i, box.lo+vec3{dist,dist,dist}});
        }
    }

    int get_in_reach(box_t box) {
        return std::count_if(pods.begin(), pods.end(), [](const auto& pod){
            return false; 
        });
    }

    std::string part2() override {
        auto max_coord = 0;
        for (auto& p : pods) {
            max_coord = std::max(max_coord, std::abs(p.pos.x + p.r));
        }
        box_t box {{-max_coord, -max_coord, -max_coord}, {max_coord, max_coord, max_coord}};
        box_t best;

        auto box_size = 1;
        while (box_size <= max_coord)
            box_size *= 2;

        int count = 0;
        int best_count = 0;
        using tuple_t = std::tuple<int, int, int, box_t>;
        std::priority_queue<tuple_t, std::vector<tuple_t>, std::function<bool(const tuple_t&, const tuple_t&)>> queue([](const tuple_t& t, const tuple_t& t2){
                auto reach1 = std::get<0>(t);
                auto reach2 = std::get<0>(t2);
                auto size1 = std::get<1>(t);
                auto size2 = std::get<1>(t2);
                auto origin1 = std::get<2>(t);
                auto origin2 = std::get<2>(t2);
                if(reach1 != reach2) return reach1>reach2;
                if(size1 != size2) return size1>size2;
                if(origin1 != origin2) return origin1<origin2;
                return false;
                
        });
        while(box_size > 1) {
            break;
        }

        return std::to_string(mdist(best.lo, vec3{0,0,0}));
    }

    void prepare(std::istream& i) override {
        std::string line;
        while (std::getline(i, line)) {
            int a, b, c, d;
            sscanf(line.c_str(), "pos = <%d, %d, %d>, r = %d", &a, &b, &c, &d);
            pods.push_back({a, b, c, d});
        }
    }
    std::vector<pod> pods;
    pod largest_pod;
};

template <>
void solve<aoc_day::day23>(const std::string& ans1, const std::string& ans2) {
    day23 d(ans1, ans2);
    d.solve();
}
