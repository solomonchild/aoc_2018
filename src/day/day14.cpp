#include "day.hpp"

#include "iteration.hpp"

class day14 : public day {
public:
  day14(const std::string& ans1, const std::string& ans2)
      : day(aoc_day::day14, ans1, ans2) {}

  inline void process(std::vector<int>& scores, int& elf1, int& elf2) {
    auto sum = scores[elf1] + scores[elf2];
    auto first = sum/10;
    auto second = sum%10;
    if (first != 0) scores.push_back(first);
    scores.push_back(second);
    auto size = scores.size();
    elf1 = (elf1 + 1 + scores[elf1]) % size;
    elf2 = (elf2 + 1 + scores[elf2]) % size;
  }

  std::string part1() override {
    std::vector<int> scores{3, 7};
    scores.reserve(recipes);
    int elf1 = 0;
    int elf2 = 1;
    while (scores.size() < recipes + 10) {
      process(scores, elf1, elf2);
    }
    std::string str;
    std::transform(std::next(scores.begin(), recipes), scores.end(), std::back_inserter(str),
                   [](int i) { return i + '0'; });
    return str;
  }

  std::string part2() override {
    int elf1 = 0;
    int elf2 = 1;
    std::vector<int> scores = {3, 7};
    scores.reserve(30'000'000);
    auto start = scores.begin();
    auto found_it = scores.end();
    while (found_it == scores.end()) {
      process(scores, elf1, elf2);
      if (scores.size() > 7) start = scores.end() - subsequence.size() - 1;
      found_it = std::search(start, scores.end(), subsequence.begin(), subsequence.end());
    }
    return std::to_string(std::distance(scores.begin(), found_it));
  }

  void prepare(std::istream& i) override {
    i >> recipes;
    int temp = recipes;
    while (temp > 0) {
      subsequence.insert(subsequence.begin(), temp % 10);
      temp /= 10;
    }
  }
  int recipes;
  std::vector<int> subsequence;
};

template <>
void solve<aoc_day::day14>(const std::string& ans1, const std::string& ans2) {
  day14 d(ans1, ans2);
  d.solve();
}
