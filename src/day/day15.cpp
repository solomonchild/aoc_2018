#include "day.hpp"

#include <cassert>
#include <map>
#include <queue>
#include <set>

struct point_t {
    int x = 0;
    int y = 0;

    bool operator==(const point_t& rhs) const { return x == rhs.x && y == rhs.y; }
    bool operator!=(const point_t& rhs) const { return !(*this==rhs); }
    bool operator<(const point_t& rhs) const {
        if(y == rhs.y)
            return x < rhs.x;
        else
            return y < rhs.y;
    }
    bool is_null() const { return x == -1 && y == -1; }
    static point_t null() { return {-1, -1}; }
};

class day15 : public day {
  public:
    day15(const std::string& ans1, const std::string& ans2)
        : day(aoc_day::day15, ans1, ans2) {}

    using row_t = std::vector<char>;
    using grid_t = std::vector<row_t>;
    using points_t = std::vector<point_t>;

    std::pair<int, bool> process() {
        auto orig_cr = creatures;
        auto p = plan;
        bool elves_died = true;
        int elves =
            std::count_if(creatures.begin(), creatures.end(), [](const auto& p) { return p.type == Entity::Elf; });
        const int elves_orig = elves;
        int goblins =
            std::count_if(creatures.begin(), creatures.end(), [](const auto& p) { return p.type == Entity::Goblin; });
        int rounds = 0;

        while(elves && goblins) {
            int moves = 0;
            for(auto& c : creatures) {
                if(!c.move(creatures)) break;
                moves++;
            }
            if(moves == creatures.size()) rounds++;
            creatures.erase(std::remove_if(creatures.begin(), creatures.end(),
                                           [&goblins, &elves](const Creature& c) {
                                               if(!c.active) {
                                                   if(c.type == Entity::Elf)
                                                       elves--;
                                                   else
                                                       goblins--;
                                               }
                                               return !c.active;
                                           }),
                            creatures.end());
            if(!elves || !goblins) { break; }
            std::sort(creatures.begin(), creatures.end());
        }
        if(put) print(creatures, plan);
        auto i = 0;
        for(const auto& c : creatures) { i += c.hp; }
        elves_died = elves != elves_orig;
        plan = p;
        creatures = orig_cr; 
        return {i * rounds, elves_died};
    }

    std::string part1() override {
        put = false;
        return std::to_string(process().first);
    }
    std::string part2() override {
        put = false;
        int res = 0;
        for(auto i : range(13, 15)) {
            for(auto& c : creatures) {
                if(c.type == Entity::Elf) { c.attack = i; }
            }
            auto p = process();
            if(!p.second) {
                res = p.first;
                break;
            }
        }
        return std::to_string(res);
    }

    void prepare(std::istream& i) override {
        std::string line;
        creatures.clear();
        while(std::getline(i, line)) { plan.emplace_back(line.begin(), line.end()); }

        int y = 0;
        for(auto& r : plan) {
            int x = 0;
            for(auto& c : r) {
                auto ent = chr_to_ent(c);
                if(ent == Entity::Elf || ent == Entity::Goblin) {
                    creatures.emplace_back(x, y, ent, plan, 3);
                    c = ent_to_char(ent);
                }
                x++;
            }
            y++;
        }
    }
    enum class Entity { Hole, Wall, Goblin, Elf, Invalid };

    inline static char ent_to_char(Entity e) {
        if(e == Entity::Hole) return '.';
        else if(e == Entity::Wall) return '#';
        else if(e == Entity::Goblin) return 'G';
        else if(e == Entity::Elf) return 'E';
        else return '?';
    }

    inline static Entity chr_to_ent(char c) {
        if(c == '.') return Entity::Hole;
        else if(c == '#') return Entity::Wall;
        else if(c == 'G') return Entity::Goblin;
        else if(c == 'E') return Entity::Elf;
        else return Entity::Invalid;
    }

    struct Creature;
    using creatures_t = std::vector<Creature>;

    friend void print(const creatures_t& cr, const grid_t& plan);
    struct Creature {
        int x = 0;
        int y = 0;
        int hp = 200;
        int attack;
        Creature(int x, int y, Entity type, grid_t& plan, int attack)
            : x(x)
            , y(y)
            , type(type)
            , plan(plan)
            , attack(attack) {}
        Creature& operator=(const Creature& other) {
            x = other.x;
            y = other.y;
            plan = other.plan;
            type = other.type;
            active = other.active;
            hp = other.hp;
            attack = other.attack;
            return *this;
        }
        bool operator==(const Creature& other) const {
            return x == other.x && y == other.y && type == other.type && hp == other.hp && active == other.active;
        }
        bool operator<(const Creature& other) const { return point_t{x, y} < point_t{other.x, other.y}; }
        bool move(creatures_t& creatures) {
            if(!active) return true;
            creatures_t all_enemies = creatures;
            std::map<point_t, points_t> cache;
            all_enemies.erase(
                std::remove_if(all_enemies.begin(), all_enemies.end(),
                               [this, &cache](const auto& p1) { return !p1.active || p1.type == type || p1 == *this; }),
                all_enemies.end());
            if(all_enemies.empty()) return false;
            points_t squares;
            squares.reserve(20);
            for(const auto& e : all_enemies) {
                auto ns = get_neighbours({e.x, e.y}, plan, creatures, {x,y});
                for(auto&& n : ns) {
                    cache[n] = std::move(shortest_path(plan, creatures, {x, y}, n));
                    if(!cache[n].empty() && !cache[n][0].is_null())
                        squares.push_back(std::move(n));
                }
            }

            if(squares.empty()) return true;

            std::sort(squares.begin(), squares.end(), [&](const auto& c1, const auto& c2) {
                const auto& p1 = cache[c1];
                const auto& p2 = cache[c2];
                auto s1 = p1.size();
                auto s2 = p2.size();
                if(s1 == s2 && s1 > 1) return *p1.rbegin() < *p2.rbegin();
                else return s1 < s2;
            });


            auto path = cache[squares[0]];

            if(path.size() > 1) {
                plan[y][x] = '.';
                auto px = x;
                auto py = y;
                x = std::next(path.begin())->x;
                y = std::next(path.begin())->y;
                plan[y][x] = ent_to_char(type);
            }
            auto n = get_enemies({x, y}, plan, all_enemies);

            if(n.empty()) return true;
            auto& creature = get_creature_by_xy(n[0], creatures);
            creature.hp -= attack;
            if(creature.hp <= 0) { creature.active = false; plan[creature.y][creature.x] = '.'; }
            return true;
        }
        grid_t& plan;
        Entity type;
        bool active = true;
    };

    inline static auto get_creature_by_xy(point_t pt, const creatures_t& creatures) -> const Creature& {
        auto it = std::find_if(creatures.begin(), creatures.end(),
                               [pt](const auto& c) { return c.x == pt.x && c.y == pt.y && c.active; });
        assert(it != creatures.end());
        return *it;
    };
    inline static auto get_creature_by_xy(point_t pt, creatures_t& creatures) -> Creature& {
        auto it = std::find_if(creatures.begin(), creatures.end(),
                               [pt](const auto& c) { return c.x == pt.x && c.y == pt.y && c.active; });
        assert(it != creatures.end());
        return *it;
    };

    static auto get_enemies(const point_t& pt, const grid_t& plan, const creatures_t& creatures) -> points_t {
        int x0 = pt.x;
        int y0 = pt.y;
        points_t pts;
        pts.reserve(4);
        auto this_type = plan[y0][x0];
        auto valid = [&creatures, pt, &plan, this_type](int x, int y) {
            return (y >= 0 && y < plan.size() && x >= 0 && x < plan[0].size() && (plan[y][x] == 'E' || plan[y][x] == 'G') &&
                    this_type != plan[y][x]);
        };
        if(valid(x0, y0 - 1)) pts.push_back({x0, y0 - 1});
        if(valid(x0 - 1, y0)) pts.push_back({x0 - 1, y0});
        if(valid(x0 + 1, y0)) pts.push_back({x0 + 1, y0});
        if(valid(x0, y0 + 1)) pts.push_back({x0, y0 + 1});
        std::sort(pts.begin(), pts.end(), [&creatures](const auto& p1, const auto& p2) {
            const auto& c1 = get_creature_by_xy(p1, creatures);
            const auto& c2 = get_creature_by_xy(p2, creatures);
            if(c1.hp == c2.hp) return p1 < p2;
            else return c1.hp < c2.hp;
        });
        return pts;
    };
    static auto get_neighbours(const point_t& pt, const grid_t& plan, const creatures_t& creatures, const point_t& cur) -> points_t {
        int x = pt.x;
        int y = pt.y;
        points_t pts;
        pts.reserve(4);
        static const auto width = plan[0].size();
        auto valid = [&creatures, &plan, &cur](int x, int y) {
            //cur is needed for the case when E or G is in attacking position
            return ((y >= 0 && y < plan.size() && x >= 0 && x < width && plan[y][x] == '.') || (cur.x == x && cur.y == y));
        };
        if(valid(x, y - 1)) pts.push_back({x, y - 1});
        if(valid(x - 1, y)) pts.push_back({x - 1, y});
        if(valid(x + 1, y)) pts.push_back({x + 1, y});
        if(valid(x, y + 1)) pts.push_back({x, y + 1});
        return pts;
    };
    static points_t shortest_path(grid_t& plan, const creatures_t& creatures, point_t start, point_t end) {
        if(plan[start.y][start.x] == '#' || plan[end.y][end.x] == '#') {
            return {point_t::null()};
        }

        std::set<point_t> closed_set;
        std::map<point_t, point_t> came_from;
        std::map<point_t, int> g_score{{start, 0}};
        auto get_score = [](const std::map<point_t, int>& score, point_t pt) {
            if(score.find(pt) == score.end()) return int(9999);
            else return score.at(pt);
        };
        std::queue<point_t> open_set;
        open_set.push(start);
        auto reconstr = [](const std::map<point_t, point_t>& came_from, point_t current) {
            points_t res{current};
            while(came_from.find(current) != came_from.end()) {
                current = came_from.at(current);
                res.insert(res.begin(), current);
            }
            return res;
        };
        while(!open_set.empty()) {
            auto cur = open_set.front();
            open_set.pop();
            if(closed_set.count(cur)) continue;
            if(cur == end) return reconstr(came_from, cur);
            closed_set.insert(cur);
            for(const auto& n : get_neighbours(cur, plan, creatures, start)) {
                if(closed_set.count(n)) continue;
                auto tent = 1 + get_score(g_score, cur);
                open_set.push(n);
                if(tent >= get_score(g_score, n)) continue;
                came_from[n] = cur;
                g_score[n] = tent;
            }
        }
        return {point_t::null()};
    }
    grid_t plan;
    creatures_t creatures;
    bool put = true;
};

static auto get_creature_by_xy(point_t pt, const day15::creatures_t& creatures) {
    auto it =
        std::find_if(creatures.begin(), creatures.end(), [pt](const auto& c) { return c.x == pt.x && c.y == pt.y; });
    assert(it != creatures.end());
    return *it;
};
void print(const day15::creatures_t& cr, const day15::grid_t& plan) {
    int y = 0;
    auto& os = std::cout;
    for(auto row : plan) {
        std::string str_to_print;
        for(auto [x, chr] : enumerate(row)) {
            os << chr;
            if(chr == 'G' || chr == 'E') {
                if(!str_to_print.empty()) str_to_print += ',';
                const auto& current = get_creature_by_xy({int(x), y}, cr);
                str_to_print += day15::ent_to_char(current.type);
                str_to_print += "(" + std::to_string(current.hp) + ")";
            }
        }
        os << " " << str_to_print << std::endl;
        y++;
    }
    os << std::endl;
}

template <>
void solve<aoc_day::day15>(const std::string& ans1, const std::string& ans2) {
    day15 d(ans1, ans2);
    d.solve();
}
