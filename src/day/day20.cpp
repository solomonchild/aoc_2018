#include "day.hpp"

#include <cassert>
#include <memory>
#include <map>
#include <deque>

struct vec2 {
    int x = 0;
    int y = 0;
    bool operator<(const vec2& other)const {
        if (y == other.y) return x < other.x;
        else return y < other.y;
    }
};

class day20 : public day {
  public:
    day20(const std::string& ans1, const std::string& ans2)
        : day(aoc_day::day20, ans1, ans2) {}

    std::string part1() override { 
        int x = 0;
        int y =0;
        int px = x;
        int py = y;
        std::deque<vec2> positions;
        int pos = 0;
        for (; pos < str.size() && str[pos] != '$'; ++pos) {
            auto c = str[pos];
            if(c == '^') continue;

            if(c == '(') { 
                positions.push_back({x,y});
            }
            else if(c == '|') { 
                auto t = positions.back();
                x = t.x;
                y = t.y;
            }
            else if(c == ')') { 
                auto t = positions.back();
                x = t.x;
                y = t.y;
                positions.pop_back();
            }
            else {
                auto d = get_pos(c);
                x+=d.x;
                y+=d.y;
                if(dist.find({x,y}) == dist.end()) {
                    dist[{x,y}] = dist[{px,py}] + 1;
                }
                else {
                    dist[{x,y}] = std::min(dist[{x,y}], dist[{px,py}]+1);
                }
            }
            std::tie(px,py) = std::tie(x,y);
        }
        return std::to_string(std::max_element(dist.begin(), dist.end(), [](const auto& p1, const auto& p2){
                return p1.second < p2.second;
        })->second);
    }

    std::string part2() override { 
        int res = 0;
        res = std::count_if(dist.begin(), dist.end(), [](const auto& p){
                return p.second >= 1000;
        });
        return std::to_string(res); 
    }

    vec2 get_pos(char p) {
        if(p == 'N') return {0, -1};
        if(p == 'S') return {0, 1};
        if(p == 'E') return {1, 0};
        if(p == 'W') return {-1, 0};
    }
    void prepare(std::istream& i) override {
        std::getline(i, str);
    }
    std::map<vec2, int> dist;
    std::string str;
};

template <>
void solve<aoc_day::day20>(const std::string& ans1, const std::string& ans2) {
    day20 d(ans1, ans2);
    d.solve();
}
