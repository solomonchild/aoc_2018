#include "day.hpp"

#include <array>
#include <cassert>
#include <cstdio>
#include <map>

class day4 : public day {
public:
  day4(const std::string& ans1, const std::string& ans2)
      : day(aoc_day::day4, ans1, ans2) {}

  std::string part1() override {
    int prev_start = 0;
    int current_id = 0;
    for (const auto& line : strings) {
      char what[20] = {};
      char arg[20] = {};
      int y, m, d, hh, mm;
      sscanf(line.c_str(), "[%d-%d-%d %d:%d] %s %s", &y, &m, &d, &hh, &mm, what, arg);

      if (std::string leftover(what); leftover.find("Guard") != std::string::npos) {
        std::sscanf(arg, "#%d", &current_id);
      } else if (std::string leftover(what); leftover.find("wakes") != std::string::npos) {
        info[current_id].total += (mm - prev_start);
        for (auto i : range(prev_start, mm)) {
          info[current_id].minute_freq[i]++;
        }
      } else {
        prev_start = mm;
      }
    }
    auto it = std::max_element(info.begin(), info.end(),
                               [](const auto& f, const auto& s) { return f.second.total < s.second.total; });
    assert(it != info.end());
    auto id = it->first;
    auto most_freq_minute = std::distance(it->second.minute_freq.cbegin(), get_max_freq_bucket(it->second));
    return std::to_string(id * most_freq_minute);
    // 14346
  }

  std::string part2() override {
    auto max_el = std::max_element(info.begin(), info.end(), [this](const auto& l, const auto& r){
        auto first = *get_max_freq_bucket(l.second);
        auto second = *get_max_freq_bucket(r.second);
        return first<second;
    });
    auto id = max_el->first;
    auto max_time = std::distance(max_el->second.minute_freq.cbegin(), get_max_freq_bucket(max_el->second));

    return std::to_string(id*max_time);
  }

  void prepare(std::istream& i) override {
    std::string line;
    while (std::getline(i, line)) {
      strings.push_back(line);
    }
    std::sort(strings.begin(), strings.end());
  }
  struct time_info {
    std::array<int, 60> minute_freq = {};
    int total = 0;
  };
  auto get_max_freq_bucket(const time_info& info) -> decltype(info.minute_freq.begin()) {
        return std::max_element(info.minute_freq.begin(), info.minute_freq.end(), [](const auto& f, const auto& s){
            return f<s;
        });
  }
  std::vector<std::string> strings;
  std::map<int, time_info> info;
};

template <>
void solve<aoc_day::day4>(const std::string& ans1, const std::string& ans2) {
  day4 d(ans1, ans2);
  d.solve();
}
