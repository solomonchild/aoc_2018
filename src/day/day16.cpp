#include "day.hpp"

#include <functional>
#include <map>
#include <cassert>
#include <set>

class day16 : public day {
public:
  day16(const std::string& ans1, const std::string& ans2)
      : day(aoc_day::day16, ans1, ans2) {}

  std::string part1() override {
    int res = 0;
    std::map<int, std::set<std::string>> undecided;
    std::set<int> closed_set;
    std::set<int> open_set{0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15};
    std::set<std::string> open_names;
    for(auto [k,v]:operations) {
        open_names.insert(k);
    }
    for(const auto& c : cases) {
        int num = 0;
        auto instr = std::get<1>(c);
        auto regs_after = std::get<5>(c);

        for(const auto& [name,op] : operations) {
            regs = std::get<0>(c);
            op(std::get<2>(c), std::get<3>(c), std::get<4>(c));
            if(regs == regs_after) {
                num++; 
                if(closed_set.count(instr) == 0 && open_names.count(name) != 0){
                    undecided[instr].insert(name);
                }
            }
        }
        if(num >= 3) res++;
    }

    while(open_set.size() > 1) {

        for(auto&[k,v]:undecided) {
            if(v.size() == 1) {
                open_set.erase(k);
                closed_set.insert(k);
                open_names.erase(*v.begin());
                opcodes[k] = *v.begin();
            }
        }
        for(auto s : closed_set) {
            for(auto&[k,v]:undecided) {
                v.erase(opcodes[s]);
            }
        }

    }
    if(open_names.size() == 1) opcodes[*open_set.begin()] = *open_names.begin();


    return std::to_string(res);
  }

  std::string part2() override {
    regs.clear();
    for(const auto& [op,a,b,c] : program) {
        assert(opcodes.find(op) != opcodes.end());
        operations[opcodes[op]](a,b,c);
    }
    return std::to_string(regs[0]);
  }

  void prepare(std::istream& i) override {
      std::string before;
      std::string instr;
      std::string after;
      std::string temp;
      while(std::getline(i, before)) {
        std::getline(i, instr);
        int reg1, reg2, reg3, reg4;
        int res = sscanf(before.c_str(), "Before: [%d, %d, %d, %d]", &reg1, &reg2, &reg3, &reg4);
        if(res<=0) break;
        std::getline(i, after);
        std::getline(i, temp);

        regs_t regs_b {{0,reg1}, {1,reg2}, {2,reg3}, {3,reg4}};


        int i1, i2, i3, i4;
        sscanf(instr.c_str(), "%d %d %d %d", &i1, &i2, &i3, &i4);

        sscanf(after.c_str(), "After: [%d, %d, %d, %d]", &reg1, &reg2, &reg3, &reg4);
        regs_t regs_a {{0,reg1}, {1,reg2}, {2,reg3}, {3,reg4}};

        cases.emplace_back(regs_b, i1, i2, i3, i4, regs_a);
      }
      std::string line;
      while(std::getline(i, line)) {
        int i1, i2, i3, i4;
        if(sscanf(line.c_str(), "%d %d %d %d", &i1, &i2, &i3, &i4) <= 0) break;
        program.emplace_back(i1, i2, i3, i4);
      }
  }
  using operation_t = std::function<void(int, int, int)>;
  using regs_t = std::map<int, int>;
  std::vector<std::tuple<regs_t, int, int, int, int, regs_t>> cases;
  regs_t regs;
  std::vector<std::tuple<int, int, int, int>> program;
  operation_t addr = [&](int A, int B, int C) { regs[C] = regs[A] + regs[B]; };
  operation_t addi = [&](int A, int B, int C) { regs[C] = regs[A] + B; };

  operation_t mulr = [&](int A, int B, int C) { regs[C] = regs[A] * regs[B]; };
  operation_t muli = [&](int A, int B, int C) { regs[C] = regs[A] * B; };
  
  operation_t banr = [&](int A, int B, int C) { regs[C] = regs[A] & regs[B]; };
  operation_t bani = [&](int A, int B, int C) { regs[C] = regs[A] & B; };

  operation_t borr = [&](int A, int B, int C) { regs[C] = regs[A] | regs[B]; };
  operation_t bori = [&](int A, int B, int C) { regs[C] = regs[A] | B; }; 

  operation_t setr = [&](int A, int, int C) { regs[C] = regs[A]; };
  operation_t seti = [&](int A, int, int C) { regs[C] = A; };

  operation_t gtir = [&](int A, int B, int C) { regs[C] = (A>regs[B]); };
  operation_t gtri = [&](int A, int B, int C) { regs[C] = (regs[A]>B); };
  operation_t gtrr = [&](int A, int B, int C) { regs[C] = (regs[A]>regs[B]); };

  operation_t eqir = [&](int A, int B, int C) { regs[C] = (A==regs[B]); };
  operation_t eqri = [&](int A, int B, int C) { regs[C] = (regs[A]==B); };
  operation_t eqrr = [&](int A, int B, int C) { regs[C] = (regs[A]==regs[B]); };

  std::map<int, std::string> opcodes;
  std::map<std::string, operation_t> operations {
    {"addr", addr},
    {"addi", addi},
    {"mulr", mulr},
    {"muli", muli},
    {"banr", banr},
    {"bani", bani},
    {"borr", borr},
    {"bori", bori},
    {"setr", setr},
    {"seti", seti},
    {"gtir", gtir},
    {"gtri", gtri},
    {"gtrr", gtrr},
    {"eqir", eqir},
    {"eqri", eqri},
    {"eqrr", eqrr}
  };

};
std::ostream& operator<<(std::ostream& os, day16::regs_t regs) {
    for(auto[k,v]: regs) {
        os << k << ":" << v << ", ";
    }
    return os;
}

template <>
void solve<aoc_day::day16>(const std::string& ans1, const std::string& ans2) {
  day16 d(ans1, ans2);
  d.solve();
}
