#include "day.hpp"
#include <unordered_map>

class day2 : public day {
public:
  day2(const std::string& ans1, const std::string& ans2) : day(aoc_day::day2, ans1, ans2) {}

  std::string part1() override {
    unsigned two = 0;
    unsigned three = 0;
    for (auto line : strings) {
      std::unordered_map<char, int> map;
      for (auto c : line) {
        if (auto [it, ins] = map.insert({c, 1}); !ins) {
          map[c]++;
        }
      }
      if (std::any_of(map.begin(), map.end(), [&two, &three](const auto& pair) { return pair.second == 2; })) two++;
      if (std::any_of(map.begin(), map.end(), [&two, &three](const auto& pair) { return pair.second == 3; })) three++;
    }
    return std::to_string(two * three);
  }

  std::string part2() override {
    for (auto i = 0; i < strings.size(); i++) {
      for (auto j = i + 1; j < strings.size(); j++) {
        size_t num_diff = 0;
        size_t idx_diff = 0;
        for (auto k = 0; k < strings[0].size(); k++) {
          if (strings[i][k] != strings[j][k]) {
            num_diff++;
            idx_diff = k;
            if (num_diff == 2) break;
          }
        }
        if (num_diff == 1) {
          return strings[j].erase(idx_diff, 1);
        }
      }
    }
  }
  void prepare(std::istream& i) override {
    std::string line;
    while (std::getline(i, line)) {
      strings.push_back(line);
    }
  }
  std::vector<std::string> strings;
};

template <>
void solve<aoc_day::day2>(const std::string& ans1, const std::string& ans2) {
  day2 d(ans1, ans2);
  d.solve();
}
