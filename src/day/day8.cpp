#include "day.hpp"

class day8 : public day {
public:
  day8(const std::string& ans1, const std::string& ans2)
      : day(aoc_day::day8, ans1, ans2) {}
  struct Node {
    std::vector<int> metadata;
    bool is_null() { return metadata.empty() && children.empty(); }
    std::vector<Node> children;
  };

  Node get_node(std::istream& is, int num_to_get) {
    Node n;
    int metadata = 0;
    if (!(is >> metadata)) return n;

    n.metadata.reserve(metadata);
    n.children.reserve(num_to_get);

    for (auto i : range(num_to_get)) {
      int c = 0;
      is >> c;
      auto node = get_node(is, c);
      if (!node.is_null()) n.children.push_back(node);
    }
    while (metadata--) {
      int m = 0;
      is >> m;
      n.metadata.push_back(m);
      total += m;
    }
    return n;
  }

  std::string part1() override { return std::to_string(total); }

  long get_sum(Node& n) {
    long res = 0;
    if (n.children.empty()) {
      auto i = std::accumulate(n.metadata.begin(), n.metadata.end(), 0);
      return i;
    }
    std::vector<int> scores;
    for (auto& c : n.children) {
      scores.push_back(get_sum(c));
    }
    for (auto i : n.metadata) {
      if (i <= scores.size()) {
        res += scores[i - 1];
      }
    }
    return res;
  }

  std::string part2() override { return std::to_string(get_sum(n)); }

  void prepare(std::istream& i) override {
    int num;
    i >> num;
    n = get_node(i, num);
  }
  Node n;
  size_t total = 0;
};

template <>
void solve<aoc_day::day8>(const std::string& ans1, const std::string& ans2) {
  day8 d(ans1, ans2);
  d.solve();
}
