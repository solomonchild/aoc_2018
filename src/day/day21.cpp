#include "day.hpp"

#include <functional>
#include <map>
#include <set>

class day21 : public day {
  public:
    day21(const std::string& ans1, const std::string& ans2)
        : day(aoc_day::day21, ans1, ans2) {}

    int ip = 0;
    void process(int num) {
        while (num-- && ip < instructions.size()) {
            storei(ip);
            const auto& instr = instructions[ip];
            operations[std::get<0>(instr)](std::get<1>(instr), std::get<2>(instr), std::get<3>(instr));
            ip = geti();
            ip++;
        }
    }

    std::string part1() override {
        ip = 0;
        while(ip != 28)
            process(1);
        return std::to_string(regs[2]);
    }

    std::string part2() override {
        ip = 0;
        regs.clear();

        std::set<int> r3_cache;
        auto last = 0;
        for (unsigned i = 0;; ++i) {
            if(ip==28) {
                if(auto[it,res] = r3_cache.insert(regs[2]); !res) {
                    break;
                } else {
                    last = regs[2];
                }
            }
            process(1);
        }
        return std::to_string(last);
    }

    char num_to_chr(int num) {
        if (num == ip_reg)
            return 'I';
        else
            return 'A' + num;
    }
    std::string ins_to_str(std::string ins, int a, int b, int c) {
        std::string res;
        if (ins == "addr")
            res = std::string() + num_to_chr(c) + "=" + num_to_chr(a) + "+" + num_to_chr(b);
        else if (ins == "addi")
            res = std::string() + num_to_chr(c) + "=" + num_to_chr(a) + "+" + std::to_string(b);
        else if (ins == "mulr")
            res = std::string() + num_to_chr(c) + "=" + num_to_chr(a) + "*" + num_to_chr(b);
        else if (ins == "muli")
            res = std::string() + num_to_chr(c) + "=" + num_to_chr(a) + "*" + std::to_string(b);
        else if (ins == "eqrr")
            res = std::string() + num_to_chr(c) + "=" + num_to_chr(a) + "==" + num_to_chr(b);
        else if (ins == "gtrr")
            res = std::string() + num_to_chr(c) + "=" + num_to_chr(a) + ">" + num_to_chr(b);

        else if (ins == "bani")
            res = std::string() + num_to_chr(c) + "=" + num_to_chr(a) + "&" + std::to_string(b);
        else if (ins == "bori")
            res = std::string() + num_to_chr(c) + "=" + num_to_chr(a) + "|" + std::to_string(b);
        else if (ins == "eqri")
            res = std::string() + num_to_chr(c) + "=" + num_to_chr(a) + "==" + std::to_string(b);

        else if (ins == "seti")
            res = std::string() + num_to_chr(c) + "=" + std::to_string(a);
        else if (ins == "setr")
            res = std::string() + num_to_chr(c) + "=" + num_to_chr(a);
        else if (ins == "gtri")
            res = std::string() + num_to_chr(c) + "=" + num_to_chr(a) + ">" + std::to_string(b);
        else if (ins == "gtir")
            res = std::string() + num_to_chr(c) + "=" + std::to_string(a) + ">" + num_to_chr(b);

        if (ins.find("set") == std::string::npos && ins.find("eq") == std::string::npos &&
            ins.find("gt") == std::string::npos) {
            auto im = *ins.rbegin() == 'i';
            if (num_to_chr(c) == num_to_chr(a))
                res = num_to_chr(c) + res.substr(3, 1) + "=" + (im ? std::to_string(b) : std::string(1, num_to_chr(b)));
            else if (num_to_chr(c) == num_to_chr(b))
                res = num_to_chr(c) + res.substr(3, 1) + "=" + (im ? std::to_string(b) : std::string(1, num_to_chr(a)));
        }
        return res;
    }

    void prepare(std::istream& i) override {
        std::string garbage;
        i >> garbage >> ip_reg;
        std::string line;
        while (std::getline(i, line)) {
            if (line.empty()) continue;
            char buf[6] = {};
            int r1, r2, r3;
            sscanf(line.c_str(), "%s %d %d %d", buf, &r1, &r2, &r3);
            log(buf);
            instructions.emplace_back(buf, r1, r2, r3);
        }
        std::vector<std::string> statements;
        for (const auto& [i, instr] : enumerate(instructions)) {
            statements.push_back(
                ins_to_str(std::get<0>(instr), std::get<1>(instr), std::get<2>(instr), std::get<3>(instr)));
        }
        int st = 0;
        for (auto& s : statements) {
            int i;
            if (sscanf(s.c_str(), "I=%d", &i) > 0) {
                s += " Goto " + std::to_string(i + 1);
            } else if (sscanf(s.c_str(), "I+=%d", &i) > 0) {
                s += " Goto " + std::to_string(i + st + 1);
            }
            st++;
        }
        for (auto [i, s] : enumerate(statements)) {
            log("{}: {}", i, s);
        }
    }

    using operation_t = std::function<void(int, int, int)>;
    using regs_t = std::map<int, int>;
    std::vector<std::tuple<regs_t, int, int, int, int, regs_t>> cases;
    regs_t regs;
    std::vector<std::tuple<int, int, int, int>> program;
    operation_t addr = [&](int A, int B, int C) { regs[C] = regs[A] + regs[B]; };
    operation_t addi = [&](int A, int B, int C) { regs[C] = regs[A] + B; };

    operation_t mulr = [&](int A, int B, int C) { regs[C] = regs[A] * regs[B]; };
    operation_t muli = [&](int A, int B, int C) { regs[C] = regs[A] * B; };

    operation_t banr = [&](int A, int B, int C) { regs[C] = regs[A] & regs[B]; };
    operation_t bani = [&](int A, int B, int C) { regs[C] = regs[A] & B; };

    operation_t borr = [&](int A, int B, int C) { regs[C] = regs[A] | regs[B]; };
    operation_t bori = [&](int A, int B, int C) { regs[C] = regs[A] | B; };

    operation_t setr = [&](int A, int, int C) { regs[C] = regs[A]; };
    operation_t seti = [&](int A, int, int C) { regs[C] = A; };

    operation_t gtir = [&](int A, int B, int C) { regs[C] = (A > regs[B]); };
    operation_t gtri = [&](int A, int B, int C) { regs[C] = (regs[A] > B); };
    operation_t gtrr = [&](int A, int B, int C) { regs[C] = (regs[A] > regs[B]); };

    operation_t eqir = [&](int A, int B, int C) { regs[C] = (A == regs[B]); };
    operation_t eqri = [&](int A, int B, int C) { regs[C] = (regs[A] == B); };
    operation_t eqrr = [&](int A, int B, int C) { regs[C] = (regs[A] == regs[B]); };

    std::map<int, std::string> opcodes;
    std::map<std::string, operation_t> operations{{"addr", addr}, {"addi", addi}, {"mulr", mulr}, {"muli", muli},
                                                  {"banr", banr}, {"bani", bani}, {"borr", borr}, {"bori", bori},
                                                  {"setr", setr}, {"seti", seti}, {"gtir", gtir}, {"gtri", gtri},
                                                  {"gtrr", gtrr}, {"eqir", eqir}, {"eqri", eqri}, {"eqrr", eqrr}};

    int ip_reg = 0;
    using instruction_t = std::tuple<std::string, int, int, int>;
    std::vector<instruction_t> instructions;

    void storei(int ip) { regs[ip_reg] = ip; }
    int geti() { return regs[ip_reg]; }
};

static std::ostream& operator<<(std::ostream& os, day21::regs_t regs) {
    for (auto [k, v] : regs) {
        os << k << ":" << v << ", ";
    }
    return os;
}

template <>
void solve<aoc_day::day21>(const std::string& ans1, const std::string& ans2) {
    day21 d(ans1, ans2);
    d.solve();
}
