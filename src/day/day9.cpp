#include "day.hpp"
#include <cassert>
#include <unordered_map>
#include <list>

class day9 : public day {
public:
  day9(const std::string& ans1, const std::string& ans2)
      : day(aoc_day::day9, ans1, ans2) {}

  auto next(std::list<unsigned>& list, std::list<unsigned>::iterator it, int idx = 1) {
      while(idx--) {
          ++it;
          if(it == list.end()) {
              it = list.begin();
          }
      }
      return it;
  }

  auto prev(std::list<unsigned>& list, std::list<unsigned>::iterator it, int idx = 1) {
      while(idx--) {
          if(it == list.begin()) {
              it = list.end();
              --it;
          }
          else 
              --it;
      }
      return it;
  }
  uint64_t get_score(unsigned no_of_players, uint64_t no_of_marbles) {
      std::unordered_map<uint64_t, uint64_t> scores;
      std::list<unsigned> marbles;
      marbles.push_back(0);
      auto current = marbles.begin();
      for(auto marble : range(1, no_of_marbles + 1)) {
          auto player = (marble%no_of_players) + 1;

          if(marble%23 == 0) {
              scores[player] += marble;
              auto it_to_erase = prev(marbles, current, 7);
              current = next(marbles, it_to_erase);
              scores[player] += *it_to_erase;
              marbles.erase(it_to_erase);
          } else {
              current = next(marbles, current, 2);
              marbles.insert(current, marble);
              current = prev(marbles, current);
          }
      }
      auto it = std::max_element(scores.begin(), scores.end(), [](const auto& p1, const auto& p2){
              return p1.second < p2.second;
      });
      assert(it != scores.end());

      return it->second; 

  }

  std::string part1() override { 
      return std::to_string(get_score(no_of_players, no_of_marbles));
  }

  std::string part2() override {
    return std::to_string(get_score(no_of_players, no_of_marbles*100));
  }

  void prepare(std::istream& i) override {
    std::string s;
    std::getline(i, s);
    std::sscanf(s.c_str(), "%d players; last marble is worth %d points", &no_of_players, &no_of_marbles);
  }
  unsigned no_of_players = 0;
  uint64_t no_of_marbles = 0;
};

template <>
void solve<aoc_day::day9>(const std::string& ans1, const std::string& ans2) {
  day9 d(ans1, ans2);
  d.solve();
}
