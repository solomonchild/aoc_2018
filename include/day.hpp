#pragma once

#include <algorithm>
#include <chrono>
#include <fstream>
#include <iostream>
#include <iterator>
#include <numeric>
#include <string>
#include <vector>

#include "iteration.hpp"
#include "log.hpp"

enum class aoc_day {
    day1 = 1,
    day2,
    day3,
    day4,
    day5,
    day6,
    day7,
    day8,
    day9,
    day10,
    day11,
    day12,
    day13,
    day14,
    day15,
    day16,
    day17,
    day18,
    day19,
    day20,
    day21,
    day22,
    day23,
    day24,
    day25
};

class day {
public:
  day(aoc_day day, const std::string& ans1="", const std::string& ans2="")
      : day_(day)
      , ans1(ans1)
      , ans2(ans2) {}
  virtual std::string part1() { return ""; }
  virtual std::string part2() { return ""; }
  void solve();
  virtual void prepare(std::istream&) {}

private:
  aoc_day day_;
  std::string ans1;
  std::string ans2;
};

template <aoc_day Day>
void solve(const std::string& ans1="", const std::string& ans2="");
