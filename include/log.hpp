#pragma once

#include <string>

template<typename T>
void log(const T& t) {
    std::cout << t << std::endl;
}

template<typename... Args>
void log(const std::string& format) {
    std::cout << format << std::endl;
}

template<typename T, typename... Args>
void log(std::string format, T arg1, Args... args) {
    auto idx_1 = format.find('{');
    if(idx_1 == std::string::npos || idx_1 == format.size()-1 || format[idx_1+1]!='}') {
        std::cout << format << std::endl;
        return;
    }
    auto before = format.substr(0, idx_1);
    std::cout << before << arg1;
    format = format.substr(idx_1+2);
    log(format, args...);
}
