#pragma once
#include <tuple>

template <typename Iterator>
class enum_iterator {
public:
  enum_iterator(size_t idx, Iterator iterator)
      : idx_(idx)
      , iterator_(iterator) {}

  std::tuple<size_t, typename Iterator::value_type> operator*() { return {idx_, *iterator_}; }

  auto operator++() {
    ++idx_;
    ++iterator_;
  }

  bool operator==(const enum_iterator<Iterator>& other) const { return iterator_ == other.iterator_; }

  bool operator!=(const enum_iterator<Iterator>& other) const { return !(iterator_ == other.iterator_); }

private:
  Iterator iterator_;
  size_t idx_;
};

template <typename Container>
class enumerator {
public:
  enumerator(Container& c)
      : container_(c) {}

  auto begin() { return enum_iterator(0u, container_.begin()); }

  auto end() { return enum_iterator(0u, container_.end()); }

private:
  Container& container_;
};

template <typename Container>
class const_enumerator {
public:
  const_enumerator(const Container& c)
      : container_(c) {}

  auto begin() { return enum_iterator(0u, container_.begin()); }

  auto end() { return enum_iterator(0u, container_.end()); }

private:
  const Container& container_;
};

template <typename Container>
enumerator<Container> enumerate(Container& c) {
  return enumerator<Container>(c);
}

template <typename Container>
const_enumerator<Container> enumerate(const Container& c) {
  return const_enumerator<Container>(c);
}

class range {
public:
  class range_iterator {
  public:
    range_iterator(int64_t current, int step)
        : current(current)
        , step(step) {}

    int64_t operator*() { return current; }
    void operator++() { current += step; }
    bool operator==(const range_iterator& other) const { return current == other.current; }
    bool operator!=(const range_iterator& other) const { return !(current == other.current); }

  private:
    int64_t current;
    int step;
  };
  range(int64_t b, int64_t e, int step=1)
      : begin_(b)
      , end_(e)
      , step_((b > e) ? -step : step) {}

  range(int64_t e)
      : begin_(0)
      , end_(e)
      , step_(1) {}

  range_iterator begin() const { return range_iterator(begin_, step_); }

  range_iterator end() const { return range_iterator(end_, step_); }

private:
  int64_t begin_;
  int64_t end_;
  int step_;
};
